﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WeaponShop.DB;
using WeaponShop.Classes;
using static WeaponShop.Classes.EFClass;

namespace WeaponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для ClientAutoWindow.xaml
    /// </summary>
    public partial class ClientAutoWindow : Window
    {
        public ClientAutoWindow()
        {
            InitializeComponent();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var authUser = context.Client.ToList().Where(i => i.FirstName == TbFirstName.Text && i.Number.Trim() == TbNumber.Text).FirstOrDefault();
            if (authUser != null)
            {
                ProductWindow productWindow = new ProductWindow(authUser);
                productWindow.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("no");
            }
        }

        private void TbNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TbNumber.Text == "Номер телефона")
            {
                TbNumber.Text = null;
            }
        }
        private void TbNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TbNumber.Text == "")
            {
                TbNumber.Text = "Номер телефона";
            }
        }

        private void TbFirstName_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TbFirstName.Text == "Фамилия")
            {
                TbFirstName.Text = null;
            }
        }
        private void TbFirstName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TbFirstName.Text == "")
            {
                TbFirstName.Text = "Фамилия";
            }
        }
    }
}
