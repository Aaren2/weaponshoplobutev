﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WeaponShop.DB;
using WeaponShop.Classes;
using static WeaponShop.Classes.EFClass;
using System.Collections.ObjectModel;

namespace WeaponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для ProductWindow.xaml
    /// </summary>
    public partial class ProductWindow : Window
    {
        private Client client;
        public ProductWindow(Client a)
        {
            client = a;
            
            InitializeComponent();
            GetProduct();
            
        }
        private void GetProduct()
        {
            ObservableCollection<Product> ProdList = new ObservableCollection<Product>(context.Product.ToList());

            LVEmp.ItemsSource = ProdList;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CartWindow cartWindow = new CartWindow(client);
            cartWindow.Show();
            this.Close();
        }

        private void BtnPlus_Click(object sender, RoutedEventArgs e)
        {
            bool seek = true;
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }
            DB.Product selectedProduct = button.DataContext as DB.Product;
            if (selectedProduct != null)
            {
                for (int i = 0; i < Classes.Cart.Products.Count; i++)
                {
                    if (Classes.Cart.Products[i] == selectedProduct)
                    {
                        Classes.Cart.Products[i].Quantity++;
                        seek = false;
                    }
                }
                if (seek)
                {
                    selectedProduct.Quantity =+ 1;
                    Classes.Cart.Products.Add(selectedProduct);           
                }
            }
            GetProduct();
        }

        private void BtnMinus_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }
            DB.Product selectedProduct = button.DataContext as DB.Product;
            if (selectedProduct != null)
            {
                for (int i = 0; i < Classes.Cart.Products.Count; i++)
                {
                    if (Classes.Cart.Products[i] == selectedProduct)
                    {
                        Classes.Cart.Products[i].Quantity--;
                    }
                }
                if (selectedProduct.Quantity == 0)
                {
                    Classes.Cart.Products.Remove(selectedProduct);
                }
            }
            GetProduct();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }
            DB.Product selectedProduct = button.DataContext as DB.Product;
            if (selectedProduct != null)
            {
                for (int i = 0; i < Classes.Cart.Products.Count; i++)
                {
                    if (Classes.Cart.Products[i] == selectedProduct)
                    {
                        Classes.Cart.Products[i].Quantity = 0;
                    }
                }
                Classes.Cart.Products.Remove(selectedProduct);
            }
            GetProduct();
        }
        
    }
}
