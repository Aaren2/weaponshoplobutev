﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WeaponShop.DB;
using WeaponShop.Classes;
using static WeaponShop.Classes.EFClass;

namespace WeaponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для ClientRegWindow.xaml
    /// </summary>
    public partial class ClientRegWindow : Window
    {
        public ClientRegWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            if (string.IsNullOrWhiteSpace(TbFirstName.Text))
            {
                MessageBox.Show("Фамилия не может быть пустой");
                return;
            }
            if (string.IsNullOrWhiteSpace(TbLastName.Text))
            {
                MessageBox.Show("Имя не может быть пустым");
                return;
            }

            if (string.IsNullOrWhiteSpace(TbNumber.Text))
            {
                MessageBox.Show("Телефон не может быть пустым");
                return;
            }
            bool result = Int64.TryParse(TbNumber.Text, out var number);
            if (result != true)
            {
                MessageBox.Show("Телефон должен быть заполнен числами");
                return;
            }


            var authUser = context.Client.ToList().Where(i => i.Number == TbNumber.Text).FirstOrDefault();
            if (authUser != null)
            {
                MessageBox.Show("Клиент уже зарегистрирован");
                return;
            }
            else
            {


                DB.Client client = new DB.Client();
                client.FirstName = TbFirstName.Text;
                client.LastName = TbLastName.Text;
                if(TbMiddleName.Text == "Отчество" || TbMiddleName.Text.Trim()=="")

                client.Number = TbNumber.Text;
               
                context.Client.Add(client);

                context.SaveChanges();

                
                ProductWindow productWindow = new ProductWindow(client);
                productWindow.Show();
                this.Close();
            }
        }

        private void TbFirstName_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TbFirstName.Text == "Фамилия")
            {
                TbFirstName.Text = null;
            }
        }
        private void TbFirstName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TbFirstName.Text == "")
            {
                TbFirstName.Text = "Фвмилия";
            }
        }


        private void TbLastName_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TbLastName.Text == "Имя")
            {
                TbLastName.Text = null;
            }
        }
        private void TbLastName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TbLastName.Text == "")
            {
                TbLastName.Text = "Имя";
            }
        }


        private void TbMiddleName_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TbMiddleName.Text == "Отчество")
            {
                TbMiddleName.Text = null;
            }
        }
        private void TbMiddleName_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TbMiddleName.Text == "")
            {
                TbMiddleName.Text = "Отчество";
            }
        }


        private void TbNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TbNumber.Text == "Номер телефона")
            {
                TbNumber.Text = null;
            }
        }
        private void TbNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TbNumber.Text == "")
            {
                TbNumber.Text = "Номер телефона";
            }
        }

    }
}
