﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WeaponShop.DB;
using WeaponShop.Classes;
using static WeaponShop.Classes.EFClass;

namespace WeaponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для EmployeeAutoWindow.xaml
    /// </summary>
    public partial class EmployeeAutoWindow : Window
    {
        public EmployeeAutoWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var authUser = context.Employe.ToList().Where(i => i.Password == TbPassword.Text && i.Number.Trim() == TbNumber.Text).FirstOrDefault();
            if (authUser != null)
            {
                Emp.employe = authUser;
                MenuWindow menuWindow = new MenuWindow();
                menuWindow.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("no");
            }
        }

        private void TbNumber_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TbNumber.Text == "Номер телефона")
            {
                TbNumber.Text = null;
            }
        }
        private void TbNumber_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TbNumber.Text == "")
            {
                TbNumber.Text = "Номер телефона";
            }
        }

        private void TbPassword_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TbPassword.Text == "Пароль")
            {
                TbPassword.Text = null;
            }
        }
        private void TbPassword_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TbPassword.Text == "")
            {
                TbPassword.Text = "Пароль";
            }
        }
    }
}
