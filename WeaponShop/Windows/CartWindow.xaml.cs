﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using WeaponShop.DB;

namespace WeaponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для CartWindow.xaml
    /// </summary>
    public partial class CartWindow : Window
    {
        private Client client;
        public CartWindow(Client a)
        {
            client = a;
            InitializeComponent();
            GetProduct();
        }

        private void GetProduct()
        {
            ObservableCollection<Product> products = new ObservableCollection<Product>(Classes.Cart.Products);

            LvCartProductList.ItemsSource = products;

            Price(products);
        }

        private void Price(ObservableCollection<Product> products)
        {
            decimal price = 0;
            foreach (var item in products)
            {
                price += item.Price * item.Quantity;

            }
            price = Math.Round(price, 2);
            TbPrice.Text = price.ToString();

        }

        private void BtnPlus_Click(object sender, RoutedEventArgs e)
        {
            bool seek = true;
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }
            DB.Product selectedProduct = button.DataContext as DB.Product;
            if (selectedProduct != null)
            {
                for (int i = 0; i < Classes.Cart.Products.Count; i++)
                {
                    if (Classes.Cart.Products[i] == selectedProduct)
                    {
                        Classes.Cart.Products[i].Quantity++;
                        seek = false;
                    }
                }
                if (seek)
                {
                    selectedProduct.Quantity = +1;
                    Classes.Cart.Products.Add(selectedProduct);
                }
            }
            GetProduct();
        }

        private void BtnMinus_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }
            DB.Product selectedProduct = button.DataContext as DB.Product;
            if (selectedProduct != null && selectedProduct.Quantity != 1)
            {
                for (int i = 0; i < Classes.Cart.Products.Count; i++)
                {
                    if (Classes.Cart.Products[i] == selectedProduct)
                    {
                        Classes.Cart.Products[i].Quantity--;
                    }
                }
            }
            GetProduct();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null)
            {
                return;
            }
            DB.Product selectedProduct = button.DataContext as DB.Product;
            if (selectedProduct != null)
            {
                for (int i = 0; i < Classes.Cart.Products.Count; i++)
                {
                    if (Classes.Cart.Products[i] == selectedProduct)
                    {
                        Classes.Cart.Products[i].Quantity = 0;
                    }
                }
                Classes.Cart.Products.Remove(selectedProduct);
            }
            GetProduct();
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ProductWindow productWindow = new ProductWindow(client);
            productWindow.Show();
            this.Close();
        }
    }
}
